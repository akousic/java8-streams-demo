import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileParser {

    private final Path path;

    public FileParser(Path path) {
        this.path = path;
    }

    public List<String> parseFile() {
        List<String> parsedLines = null;

        try
        {
            Stream<String> lines = Files.lines(path);
            parsedLines =  lines.collect(Collectors.toList());
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return parsedLines;
    }
}
