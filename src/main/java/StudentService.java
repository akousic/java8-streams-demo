import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class StudentService {
    private final List<Student> students;

    public StudentService(List<Student> students) {
        this.students = students;
    }
    public List<String> getNames() {
        return this.students.stream()
                .map(s -> s.getName())
                .collect(Collectors.toList());
    }

    public List<Student> getStudentsBornInLeapYear() {
        return this.students.stream()
                .filter(student -> student.getBirthDate().isLeapYear())
                .collect(Collectors.toList());
    }

    public List<String> getFiveNamesBySkippingFirstStudent(){
       return students.stream()
                .skip(1)
                .map(s -> s.getName())
                .limit(5)
                .peek(System.out::println)
                .collect(Collectors.toList());
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentService that = (StudentService) o;
        return Objects.equals(students, that.students);
    }

    @Override
    public int hashCode() {
        return Objects.hash(students);
    }
}
