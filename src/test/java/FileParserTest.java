import org.junit.Before;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FileParserTest {
    FileParser fileParser;

    @Before
    public void setUp() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        String path  = classLoader.getResource("sample.txt").getPath();
        fileParser = new FileParser(Paths.get(path));
    }

    @Test
    public void shouldReturnLinesInFileAsList() {
        List<String> expectedLines = new ArrayList<>(List.of(
                "this is a test line",
                "this is another test line",
                "this is third test line",
                "this is fourth test line"
        ));

        List<String> actualLines = fileParser.parseFile();

        assertEquals(expectedLines,actualLines);
    }
}
