import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class StudentServiceTest {
    List<Student> students;
    StudentService studentService;

    @Before
    public void setUp() throws Exception {
        students = new ArrayList<>(List.of(
                new Student("TestStudent1", LocalDate.of(1990, 4, 13)),
                new Student("TestStudent2", LocalDate.of(1991, 5, 14)),
                new Student("TestStudent3", LocalDate.of(1992, 6, 15)),
                new Student("TestStudent4", LocalDate.of(1993, 7, 16)),
                new Student("TestStudent5", LocalDate.of(1994, 8, 17)),
                new Student("TestStudent6", LocalDate.of(1995, 9, 18)),
                new Student("TestStudent7", LocalDate.of(1996, 9, 18))
        ));

        studentService = new StudentService(students);
    }

    @Test
    public void shouldReturnNamesOfStudentsGivenTheListOfStudents() {

        List<String> expectedNames = new ArrayList<>(List.of(
                "TestStudent1",
                "TestStudent2",
                "TestStudent3",
                "TestStudent4",
                "TestStudent5",
                "TestStudent6",
                "TestStudent7"

        ));
        assertEquals(expectedNames, studentService.getNames());

    }

    @Test
    public void shouldReturnStudentsWithLeapYearOfBirth() {
        List<Student> expectedStudents = new ArrayList<>(List.of(
                new Student("TestStudent3", LocalDate.of(1992, 6, 15)),
                new Student("TestStudent7", LocalDate.of(1996, 9, 18))

        ));
        List<Student> actualStudents = studentService.getStudentsBornInLeapYear();
        assertTrue(actualStudents.equals(expectedStudents));
    }

    @Test
    public void shouldReturnNamesOfFiveStudentsSkippingTheFirstOne() {
        List<String> expectedNames = new ArrayList<>(List.of(
                "TestStudent2",
                "TestStudent3",
                "TestStudent4",
                "TestStudent5",
                "TestStudent6"
        ));
        List<String> actualNames = studentService.getFiveNamesBySkippingFirstStudent();

        assertEquals(expectedNames, actualNames);

    }
}
